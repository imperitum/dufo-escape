// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorLattice.h"
#include "Sound/SoundAttenuation.h"

// Sets default values
ADoorLattice::ADoorLattice()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Lattice = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Lattice"));
	RootComponent = Lattice;

	CellDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Lattice Door"));
	CellDoor->SetupAttachment(Lattice);

	FSoundAttenuationSettings SoundAttenuationSettings;
	SoundAttenuationSettings.DistanceAlgorithm = EAttenuationDistanceModel::NaturalSound;
	SoundAttenuationSettings.dBAttenuationAtMax = -12.f;
	SoundAttenuationSettings.FalloffMode = ENaturalSoundFalloffMode::Continues;
	SoundAttenuationSettings.AttenuationShape = EAttenuationShape::Sphere;
	SoundAttenuationSettings.AttenuationShapeExtents = FVector(20.f, 0.f, 0.f);
	SoundAttenuationSettings.FalloffDistance = 610.f;

	OpenDoorSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Door opening"));
	OpenDoorSound->SetupAttachment(Lattice);
	OpenDoorSound->SetAutoActivate(false);
	OpenDoorSound->bOverrideAttenuation = true;
	OpenDoorSound->AttenuationOverrides = SoundAttenuationSettings;

	CloseDoorSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Door closing"));
	CloseDoorSound->SetupAttachment(Lattice);
	CloseDoorSound->SetAutoActivate(false);
	CloseDoorSound->bOverrideAttenuation = true;
	CloseDoorSound->AttenuationOverrides = SoundAttenuationSettings;
}

// Called when the game starts or when spawned
void ADoorLattice::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADoorLattice::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(Scales1) || !IsValid(Scales2)) return;

	DoorCurrentYaw = CellDoor->GetRelativeRotation().Yaw;

	if (Scales1->TotalMass > 0 && Scales1->TotalMass == Scales2->TotalMass && FMath::RoundToInt(DoorCurrentYaw) != FMath::RoundToInt(OpenDoorTargetYaw))
	{
		if (!TimeDelayPassed)
		{
			TimeDelayPassed = true;
			TimeDelta = GetWorld()->GetTimeSeconds(); //set once when opening started to calculate delay when scales animation playing
		}
		if (TimeDelayPassed && GetWorld()->GetTimeSeconds() - TimeDelta >= Scales1->AnimationTime)
		{
			OpenDoor(DeltaTime);
		}	
	}
	else if (FMath::RoundToInt(DoorCurrentYaw) != 0 && Scales1->TotalMass != Scales2->TotalMass)
	{
		CloseDoor(DeltaTime);
		TimeDelayPassed = false;
	}
}

void ADoorLattice::OpenDoor(float DeltaTime)
{
	if (!DoorOpening)
	{
		OpenDoorSound->Play();
		DoorOpening = true;
		DoorClosing = false;
	}

	float NewOpenDoorYaw = FMath::FInterpTo(DoorCurrentYaw, OpenDoorTargetYaw, DeltaTime, OpeningSpeed);
	
	CellDoor->SetRelativeRotation({ 0.f, NewOpenDoorYaw, 0.f });
}

void ADoorLattice::CloseDoor(float DeltaTime)
{
	if (!DoorClosing)
	{
		CloseDoorSound->Play();
		DoorOpening = false;
		DoorClosing = true;
	}

	float NewOpenDoorYaw = FMath::FInterpTo(DoorCurrentYaw, 0.f, DeltaTime, OpeningSpeed);

	CellDoor->SetRelativeRotation({ 0.f, NewOpenDoorYaw, 0.f });
}

