// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/AudioComponent.h"
#include "CellDoor.h"
#include "EscapeCharacter.h"
#include "StoneFlap.h"
#include "Statue_Puzzle.generated.h"

UENUM()
enum class EStatue : uint8
{
	None,
	Left,
	Right,
	Middle
};

UCLASS()
class ESCAPEFROMDUFO_API AStatue_Puzzle : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AStatue_Puzzle();

	virtual void Tick(float DeltaTime) override;

	void RotateStatue(float Val, EStatue Statue);

	UPROPERTY(BlueprintReadOnly)
	bool bStatueTaskComplited = false;

	UPROPERTY(BlueprintReadOnly)
	bool bFirstContactDone = false;

	void StopStatueSound() const;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* Frame;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* LeftStatueStand;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MiddleStatueStand;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* RightStatueStand;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* LeftStatue;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MiddleStatue;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* RightStatue;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* LeftStatueAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* LeftStatueCompleteAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* LeftStatueSadAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* MiddleStatueAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* MiddleStatueCompleteAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* MiddleStatueSadAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* RightStatueAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* RightStatueCompleteAudio;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* RightStatueSadAudio;

private:

	void CheckStatuesAngle();

	UPROPERTY(EditAnywhere)
	AStoneFlap* BigStone = nullptr;

	UPROPERTY(EditAnywhere)
	ACellDoor* CellDoor = nullptr;

	UPROPERTY(EditAnywhere)
	float RotationSpeed = 33.f;

	float LeftSavedZRotation = 0.f;
	float MiddleSavedZRotation = 0.f;
	float RightSavedZRotation = 0.f;

	AEscapeCharacter* EscapeCharacter = nullptr;
};
