// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInteractionView.h"
#include "Kismet/GameplayStatics.h"

#define OUT
#define ECC_Statue ECollisionChannel::ECC_GameTraceChannel1

// Sets default values for this component's properties
UPlayerInteractionView::UPlayerInteractionView()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SphereCollision = FCollisionShape::MakeSphere(10.f);

	TraceParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	TraceParams.bIgnoreBlocks = false;

	PlayerCharacter = Cast<AEscapeCharacter>(GetOwner());

	EscapeGameInstance = Cast<UEscapeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}


// Called when the game starts
void UPlayerInteractionView::BeginPlay()
{
	Super::BeginPlay();

	SetupInputComponent();

	FindPhysicsComponent();
}

// Called every frame
void UPlayerInteractionView::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateInteraction();
}

void UPlayerInteractionView::FindPhysicsComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s phys comp was not found"), *GetOwner()->GetName());
	}
}

void UPlayerInteractionView::UpdateInteraction()
{
	if (GetPlayerViewHit().IsValidBlockingHit() && !PhysicsHandle->GrabbedComponent)
	{
		if (InteractionComponent == nullptr) //first camera contact with statue
		{
			InteractionComponent = GetPlayerViewHit().GetComponent();
			StatuePuzzle = Cast<AStatue_Puzzle>(InteractionComponent->GetOwner()); //set the statue_puzzle actor
			ToggleGlow(true, InteractionComponent);
		}

		if (InteractionComponent != nullptr && InteractionComponent != GetPlayerViewHit().GetComponent()) //if camera is over other statue
		{
			ToggleGlow(false, InteractionComponent);
			InteractionComponent = GetPlayerViewHit().GetComponent();
			ToggleGlow(true, InteractionComponent);
		}
	}
	else if (InteractionComponent != nullptr)
	{
		ToggleGlow(false, InteractionComponent);
		InteractionComponent = nullptr;
	}
}

void UPlayerInteractionView::ToggleGlow(bool IsGlowing, UPrimitiveComponent* Comp)
{
	if (EscapeGameInstance && IsGlowing && !StatuePuzzle->bStatueTaskComplited)
	{
		EscapeGameInstance->ShowStatueWidget();
		Comp->SetRenderCustomDepth(IsGlowing);
	}
	if (EscapeGameInstance && !IsGlowing)
	{
		EscapeGameInstance->HideStatueWidget();
		Comp->SetRenderCustomDepth(IsGlowing);
	}
}

void UPlayerInteractionView::SetupInputComponent()
{
	RotationInput = GetOwner()->FindComponentByClass<UInputComponent>();

	if (RotationInput != nullptr)
	{
		RotationInput->BindAxis("Rotate", this, &UPlayerInteractionView::RotateStatue);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s input comp did not found"), *GetOwner()->GetName());
	}
}

void UPlayerInteractionView::RotateStatue(float Value)
{
	if (!StatuePuzzle) return;
	
	if (!InteractionComponent) //if camera is NOT over the statues
	{
		StatuePuzzle->StopStatueSound();
		return;
	}

	if (InteractionComponent->ComponentHasTag(FName("Left")))
	{
		StatuePuzzle->RotateStatue(Value, EStatue::Left);
	}
	else if (InteractionComponent->ComponentHasTag(FName("Middle")))
	{
		StatuePuzzle->RotateStatue(Value, EStatue::Middle);
	}
	else if (InteractionComponent->ComponentHasTag(FName("Right")))
	{
		StatuePuzzle->RotateStatue(Value, EStatue::Right);
	}
}

FHitResult UPlayerInteractionView::GetPlayerViewHit() const
{
	FHitResult Hit;

	FVector StartLocation = PlayerCharacter->GetActorLocation();

	StartLocation.Z += 64.f;

	GetWorld()->SweepSingleByObjectType(
		OUT Hit,
		StartLocation,
		GetPlayerViewPoint(),
		{ 0.f, 0.f, 0.f, 0.f },
		FCollisionObjectQueryParams(ECC_Statue),
		SphereCollision,
		TraceParams
	);

	return Hit;
}

FVector UPlayerInteractionView::GetPlayerViewPoint() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * DefaultReach;
}

