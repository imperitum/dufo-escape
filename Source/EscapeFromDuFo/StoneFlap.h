// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EscapeCharacter.h"
#include "Components/BoxComponent.h"
#include "Engine/TriggerVolume.h"
#include "StoneFlap.generated.h"

class AStatue_Puzzle;

UCLASS()
class ESCAPEFROMDUFO_API AStoneFlap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStoneFlap();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void StartMoving();
	void StopMovement();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsMovementContinues = false;

	UFUNCTION(BlueprintPure, Category = "Camera")
	float GetCameraShakeValue();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* StoneFlap;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* TriggerBox;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* StoneSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* CameraShakeCurve;

	/*UPROPERTY(EditAnywhere)
	ATriggerVolume* KillConditionVolume = nullptr;*/

private:

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	UPROPERTY(EditAnywhere)
	float MovingSpeed = 1.f;

	UPROPERTY(EditAnywhere)
	AStatue_Puzzle* StatuePuzzle = nullptr;

	UPROPERTY(EditAnywhere)
	float FlapDistance = 1000.f;

	int32 TargetXLocation;
	bool bIsPlayerOverlapped = false;

	AEscapeCharacter* EscapeCharacter = nullptr;

	void KillThePlayer();
	void TimerElapsed();
	void MovementProcess(float DeltaTime);
};
