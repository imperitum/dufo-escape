// Fill out your copyright notice in the Description page of Project Settings.


#include "Statue_Puzzle.h"

// Sets default values
AStatue_Puzzle::AStatue_Puzzle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Frame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Frame"));
	RootComponent = Frame;

	LeftStatueStand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Left Statue Stand"));
	LeftStatueStand->SetupAttachment(Frame);

	MiddleStatueStand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Middle Statue Stand"));
	MiddleStatueStand->SetupAttachment(Frame);

	RightStatueStand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Right Statue Stand"));
	RightStatueStand->SetupAttachment(Frame);

	LeftStatue = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Left Statue"));
	LeftStatue->SetupAttachment(LeftStatueStand);
	LeftStatue->ComponentTags.Add(FName("Left"));

	MiddleStatue = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Middle Statue"));
	MiddleStatue->SetupAttachment(MiddleStatueStand);
	MiddleStatue->ComponentTags.Add(FName("Middle"));

	RightStatue = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Right Statue"));
	RightStatue->SetupAttachment(RightStatueStand);
	RightStatue->ComponentTags.Add(FName("Right"));

	FSoundAttenuationSettings SoundAttenuationSettings;
	SoundAttenuationSettings.DistanceAlgorithm = EAttenuationDistanceModel::NaturalSound;
	SoundAttenuationSettings.dBAttenuationAtMax = -10.f;
	SoundAttenuationSettings.FalloffMode = ENaturalSoundFalloffMode::Continues;
	SoundAttenuationSettings.AttenuationShape = EAttenuationShape::Sphere;
	SoundAttenuationSettings.AttenuationShapeExtents = FVector(30.f, 0.f, 0.f);
	SoundAttenuationSettings.FalloffDistance = 150.f;

	LeftStatueAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Left Statue Audio"));
	LeftStatueAudio->SetupAttachment(LeftStatue);
	LeftStatueAudio->SetAutoActivate(false);
	LeftStatueAudio->bOverrideAttenuation = true;
	LeftStatueAudio->AttenuationOverrides = SoundAttenuationSettings;

	LeftStatueCompleteAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Left Statue Complete Audio"));
	LeftStatueCompleteAudio->SetupAttachment(LeftStatue);
	LeftStatueCompleteAudio->SetAutoActivate(false);
	LeftStatueCompleteAudio->bOverrideAttenuation = true;
	LeftStatueCompleteAudio->AttenuationOverrides = SoundAttenuationSettings;

	LeftStatueSadAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Left Statue Sad Audio"));
	LeftStatueSadAudio->SetupAttachment(LeftStatue);
	LeftStatueSadAudio->SetAutoActivate(false);
	LeftStatueSadAudio->bOverrideAttenuation = true;
	LeftStatueSadAudio->AttenuationOverrides = SoundAttenuationSettings;

	MiddleStatueAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Middle Statue Audio"));
	MiddleStatueAudio->SetupAttachment(MiddleStatue);
	MiddleStatueAudio->SetAutoActivate(false);
	MiddleStatueAudio->bOverrideAttenuation = true;
	MiddleStatueAudio->AttenuationOverrides = SoundAttenuationSettings;

	MiddleStatueCompleteAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Middle Statue Complete Audio"));
	MiddleStatueCompleteAudio->SetupAttachment(MiddleStatue);
	MiddleStatueCompleteAudio->SetAutoActivate(false);
	MiddleStatueCompleteAudio->bOverrideAttenuation = true;
	MiddleStatueCompleteAudio->AttenuationOverrides = SoundAttenuationSettings;

	MiddleStatueSadAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Middle Statue Sad Audio"));
	MiddleStatueSadAudio->SetupAttachment(MiddleStatue);
	MiddleStatueSadAudio->SetAutoActivate(false);
	MiddleStatueSadAudio->bOverrideAttenuation = true;
	MiddleStatueSadAudio->AttenuationOverrides = SoundAttenuationSettings;

	RightStatueAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Right Statue Audio"));
	RightStatueAudio->SetupAttachment(RightStatue);
	RightStatueAudio->SetAutoActivate(false);
	RightStatueAudio->bOverrideAttenuation = true;
	RightStatueAudio->AttenuationOverrides = SoundAttenuationSettings;

	RightStatueCompleteAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Right Statue Complete Audio"));
	RightStatueCompleteAudio->SetupAttachment(RightStatue);
	RightStatueCompleteAudio->SetAutoActivate(false);
	RightStatueCompleteAudio->bOverrideAttenuation = true;
	RightStatueCompleteAudio->AttenuationOverrides = SoundAttenuationSettings;

	RightStatueSadAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Right Statue Sad Audio"));
	RightStatueSadAudio->SetupAttachment(RightStatue);
	RightStatueSadAudio->SetAutoActivate(false);
	RightStatueSadAudio->bOverrideAttenuation = true;
	RightStatueSadAudio->AttenuationOverrides = SoundAttenuationSettings;
}

// Called when the game starts or when spawned
void AStatue_Puzzle::BeginPlay()
{
	Super::BeginPlay();
}

void AStatue_Puzzle::StopStatueSound() const
{
	if (RightStatueAudio->IsPlaying()) RightStatueAudio->Stop();
	if (MiddleStatueAudio->IsPlaying()) MiddleStatueAudio->Stop();
	if (LeftStatueAudio->IsPlaying()) LeftStatueAudio->Stop();
}

// Called every frame
void AStatue_Puzzle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStatue_Puzzle::RotateStatue(float Val, EStatue Statue)
{
	if (bStatueTaskComplited) return;

	if (Val == 0.f)
	{
		StopStatueSound();
		return;
	}
	else if (!bFirstContactDone)
	{
		EscapeCharacter = Cast<AEscapeCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
		EscapeCharacter->PlayDynamicEndSound();
		bFirstContactDone = true;
		if (BigStone)
		{
			BigStone->StartMoving();
		}
	}

	Val = Val * GetWorld()->GetDeltaSeconds() * RotationSpeed;

	switch (Statue)
	{
	case EStatue::Left:
		LeftStatue->AddRelativeRotation({ 0.f, Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		MiddleStatue->AddRelativeRotation({ 0.f, -Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		RightStatue->AddRelativeRotation({ 0.f, -Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		if (!LeftStatueAudio->IsPlaying()) LeftStatueAudio->Play();
		break;
	case EStatue::Middle:
		LeftStatue->AddRelativeRotation({ 0.f, -Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		MiddleStatue->AddRelativeRotation({ 0.f, Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		RightStatue->AddRelativeRotation({ 0.f, -Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		if (!MiddleStatueAudio->IsPlaying()) MiddleStatueAudio->Play();
		break;
	case EStatue::Right:
		LeftStatue->AddRelativeRotation({ 0.f, Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		MiddleStatue->AddRelativeRotation({ 0.f, Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		RightStatue->AddRelativeRotation({ 0.f, Val, 0.f }, false, nullptr, ETeleportType::TeleportPhysics);
		if (!RightStatueAudio->IsPlaying()) RightStatueAudio->Play();
		break;
	case EStatue::None:
		StopStatueSound();
		break;
	}

	CheckStatuesAngle();
}

void AStatue_Puzzle::CheckStatuesAngle()
{
	float LeftZRotation = FMath::Abs(LeftStatue->GetRelativeRotation().Yaw);
	float MiddleZRotation = FMath::Abs(MiddleStatue->GetRelativeRotation().Yaw);
	float RightZRotation = FMath::Abs(RightStatue->GetRelativeRotation().Yaw);

	//left statue angle check
	if (LeftZRotation < 19.f && LeftZRotation < LeftSavedZRotation) //correct direction
	{
		if (!LeftStatueCompleteAudio->IsPlaying()) LeftStatueCompleteAudio->Play();
	}
	else if (LeftStatueCompleteAudio->IsPlaying())
	{
		LeftStatueCompleteAudio->FadeOut(0.5f, 0.f);
		LeftStatueCompleteAudio->Stop();
	}

	//middle statue angle check
	if (MiddleZRotation < 19.f && MiddleZRotation < MiddleSavedZRotation) //correct direction
	{
		if (!MiddleStatueCompleteAudio->IsPlaying()) MiddleStatueCompleteAudio->Play();
	}
	else if (MiddleStatueCompleteAudio->IsPlaying())
	{
		MiddleStatueCompleteAudio->FadeOut(0.5f, 0.f);
		MiddleStatueCompleteAudio->Stop();
	}

	//right statue angle check
	if (RightZRotation < 19.f && RightZRotation < RightSavedZRotation) //correct direction
	{
		if (!RightStatueCompleteAudio->IsPlaying()) RightStatueCompleteAudio->Play();
	}
	else if (RightStatueCompleteAudio->IsPlaying())
	{
		RightStatueCompleteAudio->FadeOut(0.5f, 0.f);
		RightStatueCompleteAudio->Stop();
	}

	LeftSavedZRotation = LeftZRotation;
	MiddleSavedZRotation = MiddleZRotation;
	RightSavedZRotation = RightZRotation;

	/*All statues angle check + task check*/
	if (LeftZRotation < 19.f && MiddleZRotation < 19.f && RightZRotation < 19.f && CellDoor)
	{
		bStatueTaskComplited = true;
		StopStatueSound();
		CellDoor->RiseTheCell();
		BigStone->StopMovement();
	}
}

