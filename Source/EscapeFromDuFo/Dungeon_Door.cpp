// Fill out your copyright notice in the Description page of Project Settings.

#include "Dungeon_Door.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADungeon_Door::ADungeon_Door()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DoorFrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorFrame"));
	RootComponent = DoorFrameMesh;

	RightDoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RightDoor"));
	RightDoorMesh->SetupAttachment(DoorFrameMesh);

	LeftDoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeftDoor"));
	LeftDoorMesh->SetupAttachment(DoorFrameMesh);
	
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Door Interaction Trigger Volume"));
	TriggerVolume->SetupAttachment(DoorFrameMesh);
	TriggerVolume->SetRelativeLocation({ 0.f, 0.f, 120.f });
	TriggerVolume->SetWorldScale3D({ 11.f, 7.f, 3.f });

	DoorOpeningSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Door Opening Sound"));
	DoorOpeningSound->SetupAttachment(DoorFrameMesh);

	DoorClosingSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Door Closing Sound"));
	DoorClosingSound->SetupAttachment(DoorFrameMesh);
}

void ADungeon_Door::BeginPlay()
{
	Super::BeginPlay();
}

void ADungeon_Door::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

