// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Building_EscapeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEFROMDUFO_API ABuilding_EscapeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
