// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "EscapeGameInstance.generated.h"

class UGrabber;

UCLASS()
class ESCAPEFROMDUFO_API UEscapeGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UEscapeGameInstance(const FObjectInitializer& ObjectInitializer);

	void Init();

	UFUNCTION()
	virtual void BeginLoadingScreen(const FString& MapName);

	UFUNCTION()
	virtual void EndLoadingScreen(UWorld* InLoadedWorld);

	void ShowStatueWidget();
	void HideStatueWidget();

	void ShowGrab();
	void HideGrab();

	void ShowGrabInputs();
	void HideGrabInputs();

	void ShowDeathWidget();

	void ShowEscapeMenuWidget();
	void HideEscapeMenuWidget();

	bool bGrabWidgetShowed = false;

private: 

	APlayerController* PlayerController = nullptr;
	UGrabber* GrabberReference = nullptr;

	TSubclassOf<class UUserWidget> RotateButtonsWidgetClass;
	TSubclassOf<class UUserWidget> GrabWidgetClass;
	TSubclassOf<class UUserWidget> GrabInputsWidgetClass;
	TSubclassOf<class UUserWidget> DeathWidgetClass;
	TSubclassOf<class UUserWidget> EscapeMenuWidgetClass;
	TSubclassOf<class UUserWidget> LoadingWidgetClass;
	UUserWidget* RotateButtons = nullptr;
	UUserWidget* Grab = nullptr;
	UUserWidget* GrabInputs = nullptr;
	UUserWidget* Death = nullptr;
	UUserWidget* EscapeMenu = nullptr;
	UUserWidget* Loading = nullptr;
};