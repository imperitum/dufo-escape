// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"
#include "Dungeon_Door.h"

UOpenDoor::UOpenDoor()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();

	DungeonDoor = Cast<ADungeon_Door>(GetOwner());

	TriggerBox = Cast<UBoxComponent>(DungeonDoor->TriggerVolume);
}

void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (TriggerBox && TriggerBox->IsOverlappingActor(ActorThatOpens))
	{
		OpenDoor(DeltaTime);
		if (DungeonDoor->DoorClosingSound->IsPlaying()) DungeonDoor->DoorClosingSound->FadeOut(0.5f, 0.f);
		DoorLastOpened = GetWorld()->GetTimeSeconds();
	}
	else if (TriggerBox && GetWorld()->GetTimeSeconds() - DoorLastOpened >= DoorCloseDelay)
	{
		CloseDoor(DeltaTime);
	}

	if (!TriggerBox->IsOverlappingActor(ActorThatOpens))
	{
		if (DungeonDoor->DoorOpeningSound->IsPlaying()) DungeonDoor->DoorOpeningSound->FadeOut(0.5f, 0.f);
	}
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	if (!DungeonDoor || !DungeonDoor->RightDoorMesh || !DungeonDoor->LeftDoorMesh) return;

	if (!DoorIsOpening)
	{
		DungeonDoor->DoorOpeningSound->FadeIn(0.5f);
		DoorIsOpening = true;
		DoorIsClosing = false;
	}

	float CurrentRightDoorYaw = DungeonDoor->RightDoorMesh->GetRelativeRotation().Yaw;
	float CurrentLeftDoorYaw = DungeonDoor->LeftDoorMesh->GetRelativeRotation().Yaw;

	FRotator OpenRightDoor(0.0f, -OpenDoorTargetYaw, 0.0f);
	FRotator OpenLeftDoor(0.0f, OpenDoorTargetYaw, 0.0f);

	OpenRightDoor.Yaw = FMath::FInterpTo(CurrentRightDoorYaw, -OpenDoorTargetYaw, DeltaTime, OpenDoorSpeed);
	OpenLeftDoor.Yaw = FMath::FInterpTo(CurrentLeftDoorYaw, OpenDoorTargetYaw, DeltaTime, OpenDoorSpeed);

	DungeonDoor->RightDoorMesh->SetRelativeRotation(OpenRightDoor);
	DungeonDoor->LeftDoorMesh->SetRelativeRotation(OpenLeftDoor);
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	if (!DungeonDoor || !DungeonDoor->RightDoorMesh || !DungeonDoor->LeftDoorMesh) return;

	if (!DoorIsClosing)
	{
		DungeonDoor->DoorClosingSound->FadeIn(0.5f);
		DoorIsClosing = true;
		DoorIsOpening = false;
	}

	float CurrentRightDoorYaw = DungeonDoor->RightDoorMesh->GetRelativeRotation().Yaw;
	float CurrentLeftDoorYaw = DungeonDoor->LeftDoorMesh->GetRelativeRotation().Yaw;

	FRotator OpenRightDoor(0.0f, 0.0f, 0.0f);
	FRotator OpenLeftDoor(0.0f, 0.0f, 0.0f);

	OpenRightDoor.Yaw = FMath::FInterpTo(CurrentRightDoorYaw, 0.0f, DeltaTime, OpenDoorSpeed);
	OpenLeftDoor.Yaw = FMath::FInterpTo(CurrentLeftDoorYaw, 0.0f, DeltaTime, OpenDoorSpeed);

	DungeonDoor->RightDoorMesh->SetRelativeRotation(OpenRightDoor);
	DungeonDoor->LeftDoorMesh->SetRelativeRotation(OpenLeftDoor);
}
