// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "EscapeGameInstance.h"
#include "Components/BoxComponent.h"
#include "EscapeCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;

UCLASS()
class ESCAPEFROMDUFO_API AEscapeCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

public:
	// Sets default values for this character's properties
	AEscapeCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Val);

	void MoveRight(float Val);

	void TurnAtRate(float Rate);

	void LookUpAtRate(float Rate);

	void ShowEscapeMenu();

	void PlayFootstepsSound(float Val);

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* DungeonSound;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* StepsSound;

	UPROPERTY(EditAnywhere)
	USoundBase* StoneSteps;

	UPROPERTY(EditAnywhere)
	USoundBase* ForestSteps;

	UPROPERTY(EditAnywhere)
	USoundBase* WoodSteps;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* DynamicEndMusic;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* ForestMusic;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* StartSound;

	UPROPERTY(VisibleAnywhere)
	UAudioComponent* DeathSound;

	UPROPERTY(EditAnywhere)
	float StepDistance = 210.f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere)
	UBoxComponent* RotationBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsPlayerDied = false;

	void PlayDynamicEndSound();
	void KillCharacter();

	UFUNCTION(BlueprintCallable, Category = "Sound")
	void StopDynamicEndSound();

	UFUNCTION(BlueprintCallable, Category = "Sound")
	void PlayStartSound();

	UFUNCTION(BlueprintCallable, Category = "Sound")
	void StopDungeonSound();

	UFUNCTION(BlueprintCallable, Category = "Sound")
	void PlayForestMusic();

private:

	float MovementSpeed = 150.0f;
	float VolumeDelta = 0.025f;
	float DynamicEndMusicMaxVolume = 1.5f;
	FVector LastStepLocation;
	bool bIsWalking = false;
	bool bIsStartSoundPlayed = false;

	UEscapeGameInstance* EscapeGameInstance = nullptr;
	APlayerController* PlayerController = nullptr;
	FCollisionQueryParams TraceParams;
};
