// Fill out your copyright notice in the Description page of Project Settings.


#include "Coffin.h"
#include "Particles/ParticleSystemComponent.h"

#define OUT

// Sets default values
ACoffin::ACoffin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Coffin = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stone Coffin"));
	RootComponent = Coffin;
	Coffin->SetGenerateOverlapEvents(false);

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	TriggerBox->SetupAttachment(Coffin);
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ACoffin::OnBoxBeginOverlap);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &ACoffin::OnBoxEndOverlap);

	Boom = CreateDefaultSubobject<UAudioComponent>(TEXT("Boom Sound"));
	Boom->SetupAttachment(Coffin);
	Boom->SetAutoActivate(false);
}

// Called when the game starts or when spawned
void ACoffin::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACoffin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACoffin::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ActivateEmitter(OtherActor, false);

	if (IsAllBonesChecked() && CellDoor && bDoOnce)
	{
		CellDoor->RiseTheCell();
		Boom->Play();
		bDoOnce = false;
	}
}

void ACoffin::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ActivateEmitter(OtherActor, true);

	if (IsAllBonesChecked() && CellDoor && bDoOnce)
	{
		CellDoor->RiseTheCell();
		Boom->Play();
		bDoOnce = false;
	}
}

bool ACoffin::IsAllBonesChecked() const
{
	TArray<AActor*> BoxOverlapped;

	TriggerBox->GetOverlappingActors(OUT BoxOverlapped);

	for (AActor* Bone : RequiredBones)
	{
		if (BoxOverlapped.Find(Bone) < 0)
		{
			return false;
		}
	}

	return true;
}

void ACoffin::ActivateEmitter(AActor* Actor, bool TrueFalse)
{
	UParticleSystemComponent* Emitter;
	Emitter = Actor->FindComponentByClass<UParticleSystemComponent>();

	if (Emitter)
	{
		if (!TrueFalse)
		{
			Emitter->Deactivate();
		}
		else
		{
			Emitter->Activate(true);
		}
	}
}
