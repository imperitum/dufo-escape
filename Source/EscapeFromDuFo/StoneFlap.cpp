// Fill out your copyright notice in the Description page of Project Settings.


#include "StoneFlap.h"
#include "Statue_Puzzle.h"

// Sets default values
AStoneFlap::AStoneFlap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StoneFlap = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Big Stone"));
	RootComponent = StoneFlap;
	StoneFlap->SetSimulatePhysics(true);

	StoneSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	StoneSound->SetupAttachment(StoneFlap);
	StoneSound->SetAutoActivate(false);

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	TriggerBox->SetupAttachment(StoneFlap);
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AStoneFlap::OnBoxBeginOverlap);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &AStoneFlap::OnBoxEndOverlap);
}

// Called when the game starts or when spawned
void AStoneFlap::BeginPlay()
{
	Super::BeginPlay();
	
	EscapeCharacter = Cast<AEscapeCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

	TargetXLocation = FMath::RoundToInt(GetActorLocation().Y + FlapDistance);
}

void AStoneFlap::KillThePlayer()
{
	EscapeCharacter->KillCharacter();
}

void AStoneFlap::TimerElapsed()
{
	MovingSpeed /= 5.f;
}

// Called every frame
void AStoneFlap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsMovementContinues)
	{
		MovementProcess(DeltaTime);
	}

	if (bIsPlayerOverlapped)
	{
		//it fixes the bug with collision with player when stone is moving
		EscapeCharacter->AddMovementInput(EscapeCharacter->GetActorForwardVector(), 0.001f);
	}
}

void AStoneFlap::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bIsPlayerOverlapped = true;
}

void AStoneFlap::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bIsPlayerOverlapped = false;
}

void AStoneFlap::MovementProcess(float DeltaTime)
{
	FVector CurrentLocation = GetActorLocation();
	FVector NewLocation = CurrentLocation;
	NewLocation.Y = CurrentLocation.Y + MovingSpeed * DeltaTime;
	SetActorLocation(NewLocation);
	if (FMath::RoundToInt(GetActorLocation().Y) >= TargetXLocation)
	{
		StopMovement();
	}
}

void AStoneFlap::StopMovement()
{
	StoneSound->FadeOut(0.5f, 0.f);
	bIsMovementContinues = false;
	bIsPlayerOverlapped = false;
	if (!StatuePuzzle->bStatueTaskComplited)
	{
		EscapeCharacter->KillCharacter();
	}
}

void AStoneFlap::StartMoving()
{
	StoneSound->Play();
	bIsMovementContinues = true;
	MovingSpeed *= 3.f;
	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(
	UnusedHandle, this, &AStoneFlap::TimerElapsed, 3.f, false);
}

float AStoneFlap::GetCameraShakeValue()
{
	return FMath::Clamp(CameraShakeCurve->GetFloatValue((GetActorLocation() - EscapeCharacter->GetActorLocation()).Size()), 0.f, 0.001f);
}

