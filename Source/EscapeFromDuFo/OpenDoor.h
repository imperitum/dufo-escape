// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Dungeon_Door.h"
#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEFROMDUFO_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);

private:
	
	ADungeon_Door* DungeonDoor = nullptr;

	UBoxComponent* TriggerBox = nullptr;

	AActor* ActorThatOpens = nullptr;

	bool DoorIsOpening = false;
	bool DoorIsClosing = true;

	UPROPERTY(EditAnywhere)
	float OpenDoorTargetYaw = 80.0f;

	UPROPERTY(EditAnywhere)
	float OpenDoorSpeed = 2.0f;

	float DoorLastOpened = 0.f;

	UPROPERTY(EditAnywhere)
	float DoorCloseDelay = 2.f;
};
