// Fill out your copyright notice in the Description page of Project Settings.


#include "Scales.h"

#define OUT

// Sets default values
AScales::AScales()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Base = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Scales Base"));
	RootComponent = Base;

	Trunk = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Scales Trunk"));
	Trunk->SetupAttachment(Base);

	Bowl = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Scales Bowl"));
	Bowl->SetupAttachment(Trunk);

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	Box->SetupAttachment(Trunk);

	Box->OnComponentBeginOverlap.AddDynamic(this, &AScales::OnBoxBeginOverlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &AScales::OnBoxEndOverlap);
}

// Called when the game starts or when spawned
void AScales::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AScales::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentTrunkLocation != TargetTrunkLocation)
	{
		SetNewTrunkLocation(DeltaTime);
	}
}

void AScales::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	TotalMass += OtherActor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	
	TargetTrunkLocation = FMath::Clamp(39.f - TotalMass * 7.f, -3.f, 39.f); 
}

void AScales::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	TotalMass -= OtherActor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	
	TargetTrunkLocation = FMath::Clamp(39.f - TotalMass * 7.f, -3.f, 39.f);
}

void AScales::SetNewTrunkLocation(float DeltaTime)
{
	FVector NewTrunkLocation = Trunk->GetRelativeLocation();

	NewTrunkLocation.Z = FMath::FInterpTo(CurrentTrunkLocation, TargetTrunkLocation, DeltaTime, AnimationTime);;

	Trunk->SetRelativeLocation(NewTrunkLocation);

	CurrentTrunkLocation = Trunk->GetRelativeLocation().Z;
}

