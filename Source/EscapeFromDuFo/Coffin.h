// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "CellDoor.h"
#include "Coffin.generated.h"

UCLASS()
class ESCAPEFROMDUFO_API ACoffin : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoffin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Coffin;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* TriggerBox;

	UPROPERTY(EditAnywhere)
	TArray<AActor*> RequiredBones;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UAudioComponent* Boom;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	bool IsAllBonesChecked() const;
	void ActivateEmitter(AActor* Actor, bool TrueFalse);

	bool bDoOnce = true;

	UPROPERTY(EditAnywhere)
	ACellDoor* CellDoor = nullptr;
};
