// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Private/KismetTraceUtils.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SphereCollision = FCollisionShape::MakeSphere(25.f);

	TraceParams = FCollisionQueryParams(FName(TEXT("")), true, GetOwner());

	PlayerCharacter = Cast<AEscapeCharacter>(GetOwner());

	EscapeGameInstance = Cast<UEscapeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsComponent();

	SetupInputComponent();
}

void UGrabber::FindPhysicsComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s phys comp was not found"), *GetOwner()->GetName());
	}
}

void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (IsValid(InputComponent))
	{
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::GrabReleased);
		InputComponent->BindAxis("Grab Zoom", this, &UGrabber::GrabedObjectZoom);
		InputComponent->BindAxis("Rotate", this, &UGrabber::RotateGrabbedObject);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s input comp did not found"), *GetOwner()->GetName());
	}
}

void UGrabber::RotateGrabbedObject(float Value)
{
	if (GrabbedComponent)
	{
		Value *= RotationSpeed;
		if (Value < 0.f)
		{
			GrabbedComponent->AddLocalRotation({ 0.f, Value, 0.f }, true, nullptr, ETeleportType::TeleportPhysics);
		} 
		else
		{
			GrabbedComponent->AddLocalRotation({ Value, 0.f, 0.f }, true, nullptr, ETeleportType::TeleportPhysics);
		}
	}
}

void UGrabber::Grab()
{
	FHitResult HitResult = GetPlayerViewHit(ECollisionChannel::ECC_PhysicsBody, Reach);
	AActor* ActorHit = HitResult.GetActor();

	if (ActorHit)
	{
		if (HitResult.GetComponent())
		{
			GrabLocation = HitResult.GetComponent()->GetCenterOfMass(NAME_None);
		}
		else
		{
			GrabLocation = HitResult.GetActor()->GetActorLocation();
		}

		Reach = FVector::Distance(GrabLocation, GetOwner()->GetActorLocation());

		if (!PhysicsHandle) return;

		PhysicsHandle->GrabComponentAtLocation(
			HitResult.GetComponent(),
			NAME_None,
			GrabLocation
		);

		GrabbedComponent = PhysicsHandle->GrabbedComponent;

		EscapeGameInstance->ShowGrabInputs();
		EscapeGameInstance->HideGrab();
	}
}

void UGrabber::GrabReleased()
{
	if (PhysicsHandle && GrabbedComponent)
	{
		PhysicsHandle->ReleaseComponent();
		Reach = DefaultReach;
		GrabbedComponent = nullptr;
		EscapeGameInstance->HideGrabInputs();
	}
}

void UGrabber::GrabedObjectZoom(float Value)
{
	if (Value != 0.f && GrabbedComponent)
	{
		float TestReach = FMath::Clamp(Reach + Value * 15.f, 80.f, 400.f);
		if (!GetPlayerViewHit(ECC_WorldStatic, TestReach).IsValidBlockingHit())
		{
			Reach = FMath::Clamp(Reach + Value * 15.f, 80.f, 400.f);
		}
	}
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!GrabbedComponent)
	{
		if (GetPlayerViewHit(ECollisionChannel::ECC_PhysicsBody, Reach).GetActor() && !EscapeGameInstance->bGrabWidgetShowed)
		{
			EscapeGameInstance->ShowGrab();
		}
		else if (!GetPlayerViewHit(ECollisionChannel::ECC_PhysicsBody, Reach).GetActor() && EscapeGameInstance->bGrabWidgetShowed)
		{
			EscapeGameInstance->HideGrab();
		}
	}
	else
	{
		PhysicsHandle->SetTargetLocation(GetPlayerViewPoint(Reach));
	}
}

FHitResult UGrabber::GetPlayerViewHit(ECollisionChannel CollisionChannel, float HitReach) const
{
	FHitResult Hit;

	FVector StartLocation = PlayerCharacter->GetActorLocation();

	StartLocation.Z += 80.f;

	GetWorld()->SweepSingleByObjectType(
		OUT Hit, 
		StartLocation,
		GetPlayerViewPoint(HitReach),
		{ 0.f, 0.f, 0.f, 0.f },
		FCollisionObjectQueryParams(CollisionChannel),
		SphereCollision, 
		TraceParams
	);

	return Hit;
}

FVector UGrabber::GetPlayerViewPoint(float ViewPointReach) const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * ViewPointReach;
}
