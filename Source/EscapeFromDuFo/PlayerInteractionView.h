// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EscapeCharacter.h"
#include "CollisionShape.h"
#include "CollisionQueryParams.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Statue_Puzzle.h"
#include "EscapeGameInstance.h"
#include "PlayerInteractionView.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEFROMDUFO_API UPlayerInteractionView : public UActorComponent
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	UPlayerInteractionView();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	UPROPERTY(EditAnywhere)
	float DefaultReach = 220.f;

	void UpdateInteraction();
	void ToggleGlow(bool IsGlowing, UPrimitiveComponent* Comp);
	void SetupInputComponent();
	void RotateStatue(float Val);
	void FindPhysicsComponent();

	AStatue_Puzzle* StatuePuzzle = nullptr;
	UEscapeGameInstance* EscapeGameInstance = nullptr;
	UInputComponent* RotationInput = nullptr;
	AEscapeCharacter* PlayerCharacter = nullptr;
	FCollisionQueryParams TraceParams;
	FCollisionShape SphereCollision;
	FHitResult GetPlayerViewHit() const;
	FVector GetPlayerViewPoint() const;
	UPrimitiveComponent* InteractionComponent = nullptr;
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
};
