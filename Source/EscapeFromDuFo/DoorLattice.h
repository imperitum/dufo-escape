// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TriggerVolume.h"
#include "Scales.h"
#include "Components/AudioComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "DoorLattice.generated.h"

UCLASS()
class ESCAPEFROMDUFO_API ADoorLattice : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ADoorLattice();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Lattice;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* CellDoor;

	UPROPERTY(EditAnywhere)
	UAudioComponent* CloseDoorSound;

	UPROPERTY(EditAnywhere)
	UAudioComponent* OpenDoorSound;

private:

	UPROPERTY(EditAnywhere)
	AScales* Scales1 = nullptr;

	UPROPERTY(EditAnywhere)
	AScales* Scales2 = nullptr;

	UPROPERTY(EditAnywhere)
	float OpenDoorTargetYaw = 90.f;

	UPROPERTY(EditAnywhere)
	float OpeningSpeed = 1.5f;

	float DoorCurrentYaw = 0.f;

	bool TimeDelayPassed = false;

	bool DoorOpening = false;
	bool DoorClosing = true;

	float TimeDelta = 0.f;

	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
};
