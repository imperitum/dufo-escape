// Fill out your copyright notice in the Description page of Project Settings.


#include "EscapeGameInstance.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "MoviePlayer/Public/MoviePlayer.h"
#include "TimerManager.h"
#include "Grabber.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

UEscapeGameInstance::UEscapeGameInstance(const FObjectInitializer& ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> RotateButtonsWidget (TEXT("/Game/MedievalDungeon/UI/W_RotateButtons"));
	if (!ensure(RotateButtonsWidget.Class != nullptr)) return;
	RotateButtonsWidgetClass = RotateButtonsWidget.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> GrabWidget(TEXT("/Game/MedievalDungeon/UI/W_Grab"));
	if (!ensure(GrabWidget.Class != nullptr)) return;
	GrabWidgetClass = GrabWidget.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> GrabInputsWidget(TEXT("/Game/MedievalDungeon/UI/W_GrabInputs"));
	if (!ensure(GrabInputsWidget.Class != nullptr)) return;
	GrabInputsWidgetClass = GrabInputsWidget.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> DeathWidget(TEXT("/Game/MedievalDungeon/UI/W_Death"));
	if (!ensure(DeathWidget.Class != nullptr)) return;
	DeathWidgetClass = DeathWidget.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> EscapeMenuWidget(TEXT("/Game/MedievalDungeon/UI/W_EscapeMenu"));
	if (!ensure(EscapeMenuWidget.Class != nullptr)) return;
	EscapeMenuWidgetClass = EscapeMenuWidget.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> LoadingWidget(TEXT("/Game/MedievalDungeon/UI/W_Loading"));
	if (!ensure(LoadingWidget.Class != nullptr)) return;
	LoadingWidgetClass = LoadingWidget.Class;
}

void UEscapeGameInstance::Init()
{
	PlayerController = GetFirstLocalPlayerController();
	Death = nullptr;

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UEscapeGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UEscapeGameInstance::EndLoadingScreen);
}

void UEscapeGameInstance::BeginLoadingScreen(const FString& MapName)
{
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = true;
		LoadingScreen.MinimumLoadingScreenDisplayTime = 2.f;

		Loading = CreateWidget<UUserWidget>(this, LoadingWidgetClass);
		LoadingScreen.WidgetLoadingScreen = Loading->TakeWidget();

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
}

void UEscapeGameInstance::EndLoadingScreen(UWorld* InLoadedWorld)
{

}

void UEscapeGameInstance::ShowStatueWidget()
{
	HideGrabInputs();
	RotateButtons = CreateWidget<UUserWidget>(this, RotateButtonsWidgetClass);
	RotateButtons->AddToViewport();
}

void UEscapeGameInstance::HideStatueWidget()
{
	if (RotateButtons && RotateButtons->IsInViewport())
	{
		RotateButtons->RemoveFromParent();
		RotateButtons = nullptr;
	}
}

void UEscapeGameInstance::ShowGrab()
{
	Grab = CreateWidget<UUserWidget>(this, GrabWidgetClass);
	Grab->AddToViewport();
	bGrabWidgetShowed = true;
}

void UEscapeGameInstance::HideGrab()
{
	if (Grab && Grab->IsInViewport())
	{
		Grab->RemoveFromParent();
		Grab = nullptr;
	}
	bGrabWidgetShowed = false;
}

void UEscapeGameInstance::ShowGrabInputs()
{
	GrabInputs = CreateWidget<UUserWidget>(this, GrabInputsWidgetClass);
	GrabInputs->AddToViewport();
}

void UEscapeGameInstance::HideGrabInputs()
{
	if (GrabInputs && GrabInputs->IsInViewport())
	{
		GrabInputs->RemoveFromParent();
		GrabInputs = nullptr;
	}
}

void UEscapeGameInstance::ShowDeathWidget()
{
	Death = CreateWidget<UUserWidget>(this, DeathWidgetClass);
	Death->AddToViewport();

	/*Creating input data*/
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(Death->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->SetShowMouseCursor(true);
}

void UEscapeGameInstance::ShowEscapeMenuWidget()
{
	if (Death && Death->IsInViewport()) return;

	HideGrab();
	HideStatueWidget();
	HideGrabInputs();

	EscapeMenu = CreateWidget<UUserWidget>(this, EscapeMenuWidgetClass);
	EscapeMenu->AddToViewport();
	EscapeMenu->SetKeyboardFocus();

	/*Creating input data*/
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(EscapeMenu->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->SetShowMouseCursor(true);
	PlayerController->SetPause(true);
}

void UEscapeGameInstance::HideEscapeMenuWidget()
{
	if (EscapeMenu && EscapeMenu->IsInViewport())
	{
		EscapeMenu->RemoveFromParent();
		PlayerController->SetShowMouseCursor(false);
		PlayerController->SetPause(false);
		EscapeMenu = nullptr;
	}
}