// Fill out your copyright notice in the Description page of Project Settings.


#include "CellDoor.h"

// Sets default values
ACellDoor::ACellDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Cell = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cell Door"));
	Cell->SetupAttachment(RootComponent);

	CellOpeningSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Cell opening"));
	CellOpeningSound->SetupAttachment(Cell);
	CellOpeningSound->SetAutoActivate(false);
	CellOpeningSound->bOverrideAttenuation = true;
}

// Called when the game starts or when spawned
void ACellDoor::BeginPlay()
{
	Super::BeginPlay();

	SetupTargetZLocation();
}

// Called every frame
void ACellDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (OpenTheCell && FMath::RoundToInt(CurrentZLocation) != FMath::RoundToInt(TargetLocation.Z))
	{
		TickOpenCell(DeltaTime);
	}
}

void ACellDoor::RiseTheCell()
{
	OpenTheCell = true;

	CellOpeningSound->Play();
}

void ACellDoor::TickOpenCell(float DeltaTime)
{	
	FVector NewCellLocation = Cell->GetComponentLocation();
	
	if (NewCellLocation.Z < TargetLocation.Z)
	{
		NewCellLocation.Z += CellRiseSpeed * DeltaTime;
	}

	Cell->SetWorldLocation(NewCellLocation);
}

void ACellDoor::SetupTargetZLocation()
{
	TargetLocation = Cell->GetComponentLocation();

	TargetLocation.Z += DeltaTargetZLocation;
}

