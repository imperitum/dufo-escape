// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "EscapeCharacter.h"
#include "EscapeGameInstance.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEFROMDUFO_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void GrabReleased();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere)
	float DefaultReach = 220.f;

	float RotationSpeed = 2.1f;
		
	float Reach = DefaultReach;

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;
	AEscapeCharacter* PlayerCharacter = nullptr;
	UEscapeGameInstance* EscapeGameInstance = nullptr;
	UPrimitiveComponent* GrabbedComponent = nullptr;

	void Grab();
	void GrabedObjectZoom(float Value);
	void FindPhysicsComponent();
	void SetupInputComponent();
	void RotateGrabbedObject(float Value);

	FCollisionQueryParams TraceParams;

	FCollisionShape SphereCollision;

	FHitResult GetPlayerViewHit(ECollisionChannel CollisionChannel, float HitReach) const;

	FVector GetPlayerViewPoint(float ViewPointReach) const;
	FVector GrabLocation;
	FRotator GrabRotation;
};
