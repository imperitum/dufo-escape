// Fill out your copyright notice in the Description page of Project Settings.


#include "EscapeCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Private/KismetTraceUtils.h"
#include "GameFramework/Character.h"
#include "UObject/WeakObjectPtrTemplates.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

#define OUT

// Sets default values
AEscapeCharacter::AEscapeCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(0.f, 0.f, 100.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	RotationBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Rotation Box"));
	RotationBox->SetupAttachment(RootComponent);

	DungeonSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Dungeon Sound"));
	DungeonSound->SetupAttachment(GetCapsuleComponent());

	StepsSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Stone Steps Sound"));
	StepsSound->SetupAttachment(GetCapsuleComponent());
	StepsSound->SetAutoActivate(false);

	DynamicEndMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("End sound"));
	DynamicEndMusic->SetupAttachment(GetCapsuleComponent());
	DynamicEndMusic->SetAutoActivate(false);

	ForestMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("Forest Music"));
	ForestMusic->SetupAttachment(GetCapsuleComponent());
	ForestMusic->SetAutoActivate(false);

	StartSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Start Music"));
	StartSound->SetupAttachment(GetCapsuleComponent());
	StartSound->SetAutoActivate(false);

	DeathSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Death Sound"));
	DeathSound->SetupAttachment(GetCapsuleComponent());
	DeathSound->SetAutoActivate(false);
	DeathSound->SetVolumeMultiplier(0.7f);

	EscapeGameInstance = Cast<UEscapeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	TraceParams = FCollisionQueryParams("Detection", true, this);
	TraceParams.bReturnPhysicalMaterial = true;
}

// Called when the game starts or when spawned
void AEscapeCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	LastStepLocation = GetActorLocation();
	EscapeGameInstance->Init();
}

void AEscapeCharacter::MoveForward(float Val)
{
	if (Val != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Val * GetWorld()->GetDeltaSeconds() * MovementSpeed);
	}
	
	PlayFootstepsSound(Val);
}

void AEscapeCharacter::MoveRight(float Val)
{
	if (Val != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Val * GetWorld()->GetDeltaSeconds() * MovementSpeed);
	}

	PlayFootstepsSound(Val);
}

void AEscapeCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AEscapeCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AEscapeCharacter::ShowEscapeMenu()
{
	if (PlayerController->IsPaused())
	{
		EscapeGameInstance->HideEscapeMenuWidget();
	}
	else
	{
		EscapeGameInstance->ShowEscapeMenuWidget();
	}
}

void AEscapeCharacter::PlayFootstepsSound(float Val)
{
	float DeltaStepDistance = FVector::Distance(LastStepLocation, GetActorLocation()); //calculate distance between the steps

	if (Val != 0.f && DeltaStepDistance >= StepDistance)
	{
		FHitResult Hit;

		FVector StartLocation = GetActorLocation();
		FVector EndLocation = StartLocation;
		EndLocation.Z -= 120.f;
		
		//line trace to get surface under the player
		GetWorld()->LineTraceSingleByChannel(
			OUT Hit,
			StartLocation,
			EndLocation,
			ECC_WorldStatic, 
			TraceParams
		);

		if (Hit.IsValidBlockingHit())
		{
			UPhysicalMaterial* PhysMaterial = Hit.PhysMaterial.Get();
			EPhysicalSurface SurfaceType = PhysMaterial->SurfaceType;
			
			switch (SurfaceType)
			{
			default:
				StepsSound->SetSound(StoneSteps);
				break;
			case EPhysicalSurface::SurfaceType1: //stone floor
				StepsSound->SetSound(StoneSteps);
				break;
			case EPhysicalSurface::SurfaceType2: //ground floor
				StepsSound->SetSound(ForestSteps);
				break;
			case EPhysicalSurface::SurfaceType3: //wood floor
				StepsSound->SetSound(WoodSteps);
				break;
			}
		}

		StepsSound->SetVolumeMultiplier(FMath::RandRange(0.15f, 0.4f)); //steps volume range
		StepsSound->Play();
		bIsWalking = true;
		LastStepLocation = GetActorLocation();
	}
}

void AEscapeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEscapeCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Escape", IE_Pressed, this, &AEscapeCharacter::ShowEscapeMenu);

	PlayerInputComponent->BindAxis("MoveForward", this, &AEscapeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AEscapeCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void AEscapeCharacter::PlayDynamicEndSound()
{
	DynamicEndMusic->FadeIn(2.f, 0.15f);
}

void AEscapeCharacter::StopDynamicEndSound()
{
	DynamicEndMusic->FadeOut(1.5f, 0.f);
}

void AEscapeCharacter::PlayStartSound()
{
	if (!bIsStartSoundPlayed)
	{
		bIsStartSoundPlayed = true;
		StartSound->FadeIn(1.5f, 1.f);
	}
}

void AEscapeCharacter::StopDungeonSound()
{
	DungeonSound->FadeOut(1.5f, 0.f);
}

void AEscapeCharacter::PlayForestMusic()
{
	ForestMusic->FadeIn(1.5f, 0.7f);
}

void AEscapeCharacter::KillCharacter()
{
	DeathSound->Play();
	StopDynamicEndSound();
	DisableInput(GetWorld()->GetFirstPlayerController());
	bIsPlayerDied = true;
	EscapeGameInstance->ShowDeathWidget();
}

