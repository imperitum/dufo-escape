// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/AudioComponent.h"
#include "CellDoor.generated.h"

UCLASS()
class ESCAPEFROMDUFO_API ACellDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACellDoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Cell;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UAudioComponent* CellOpeningSound;

	UPROPERTY(EditAnywhere)
	float DeltaTargetZLocation = 400.f;

	UPROPERTY(EditAnywhere)
	float CellRiseSpeed = 0.55f;

	void RiseTheCell();

private:

	FVector TargetLocation;

	float CurrentZLocation;

	bool OpenTheCell = false;

	void TickOpenCell(float DeltaTime);

	void SetupTargetZLocation();

};
